import './App.scss';
import React from "react";
import ProductList from "./components/ProductList";


class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            products: []
        };
    }

    componentDidMount() {
        fetch('products.json')
            .then(r => r.json())
            .then(r => {
                this.setState({products: r.products});
            });
    }

    render() {
        return (
            <div className="App">
                <ProductList products={this.state.products}/>
            </div>
        );
    }
}

export default App;
