class StorageService {
    constructor() {
        this.addedProducts = 'addedProducts';
        this.favoriteProducts = 'favoriteProducts';
        this.storage = localStorage;
    }

    getFavorites(id) {
        let favoriteProducts = JSON.parse(this.storage.getItem(this.favoriteProducts)) || [];
        const index = favoriteProducts.findIndex(v => v === id);
        return [favoriteProducts, index];
    }

    addOrRemoveFavorite(id) {
        const [favoriteProducts, index] = this.getFavorites(id);
        let newFavState = index === -1;

        if (newFavState) {
            favoriteProducts.push(id);
        } else {
            favoriteProducts.splice(index, 1);
        }
        this.storage.setItem(this.favoriteProducts, JSON.stringify(favoriteProducts.sort((a, b) => a - b)));
        return newFavState;
    }

    addToCart(id) {
        const addedProducts = JSON.parse(this.storage.getItem(this.addedProducts)) || [];
        addedProducts.push(id);
        this.storage.setItem(this.addedProducts, JSON.stringify(addedProducts));
    }
}

export default new StorageService();