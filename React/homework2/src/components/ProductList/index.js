import React from 'react';
import PropTypes from 'prop-types';
import Product from "../Product";
import './productList.scss';

class ProductList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className='product-list'>
                {this.props.products.map(product => <Product product={product} key={product.id}/>)}
            </div>
        );
    }
}

ProductList.propTypes = {
    products: PropTypes.arrayOf(PropTypes.object).isRequired,
}

export default ProductList;