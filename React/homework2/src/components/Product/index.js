import React from "react";
import PropTypes from 'prop-types';
import "./product.scss";
import Button from "../Button";
import Modal from "../Modal";
import StorageService from "../services/StorageService";

class Product extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            modalOpened: false,
            favorite: false
        }

        this.closeModal = this.closeModal.bind(this);
        this.addToCart = this.addToCart.bind(this);
        this.onFavoriteClick = this.onFavoriteClick.bind(this);
    }

    componentDidMount() {
        let [, index] = StorageService.getFavorites(this.props.product.id);
        this.setState({favorite: index !== -1})
    }

    addToCart() {
        StorageService.addToCart(this.props.product.id);

        this.setState({modalOpened: true});
    }

    closeModal() {
        this.setState({modalOpened: false});
    }

    onFavoriteClick() {
        const newFavState = StorageService.addOrRemoveFavorite(this.props.product.id);

        this.setState({favorite: newFavState});
    }

    render() {
        return (
            <div className='product-tile product-list-tile'>
                <div className='product-tile-img-container'>
                    <img className='product-tile-img' src={this.props.product.img} alt={this.props.product.name}/>
                </div>
                <div className="product-tile-wrapper">
                    <div className="product-tile-info">
                        <p className='product-tile-id'><span>ID:</span> {this.props.product.id}</p>
                        <p className='product-tile-name'>{this.props.product.name}</p>
                        <p className='product-tile-price'>{this.props.product.price} UAH</p>
                    </div>
                    <div className="product-tile-actions">
                        <Button text='Add to Cart' onClick={this.addToCart} backgroundColor='green'/>
                        <Button text={this.state.favorite ? <i className="fas fa-star"/> : <i className="far fa-star"/>}
                                onClick={this.onFavoriteClick} backgroundColor='greenyellow'/>
                    </div>
                </div>

                {this.state.modalOpened ? (
                    <Modal
                        id='1'
                        header='The item was added to cart!'
                        text={`Your item ${this.props.product.name} was added to cart.`}
                        actions={
                            [
                                <Button
                                    key='Close'
                                    text='Close'
                                    backgroundColor={'rgba(0, 0, 0, 0.1)'}
                                    onClick={this.closeModal}
                                />
                            ]
                        }
                        closeButton={true}
                        onClose={this.closeModal}
                    />
                ) : null}
            </div>
        )
    }
}

Product.propTypes = {
    product: PropTypes.shape({
        img: PropTypes.string,
        name: PropTypes.string,
        price: PropTypes.number,
        id: PropTypes.number
    }).isRequired
}

export default Product;