import React from "react";
import PropTypes from 'prop-types';
import './button.scss';

class Button extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <button className='btn' style={{backgroundColor: this.props.backgroundColor}}
                    onClick={this.props.onClick}>{this.props.text}</button>
        )
    }
}

Button.propTypes = {
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    backgroundColor: PropTypes.string
};

export default Button;