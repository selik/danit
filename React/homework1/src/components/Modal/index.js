import React from 'react';
import PropTypes from 'prop-types';
import './modal.scss';

class Modal extends React.Component {
    constructor(props) {
        super(props);
        this.onClose = this.onClose.bind(this);
    }

    onClose() {
        this.props.onClose();
    }

    render() {
        return (
            <div className="modal">
                <div className="modal-overlay" onClick={this.onClose}/>
                <div className="modal-block">
                    <div className="modal-header">
                        <span className='modal-header-text'>{this.props.header}</span>
                        {this.props.closeButton ?
                            <button className='close' onClick={this.onClose}>X</button>
                            : null}
                    </div>
                    <div className="modal-content">
                        {this.props.text}
                    </div>
                    <div className="modal-actions">
                        {this.props.actions}
                    </div>
                </div>
            </div>
        );
    }
}

Modal.propTypes = {
    header: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    actions: PropTypes.arrayOf(PropTypes.object).isRequired,
    onClose: PropTypes.func.isRequired,
    closeButton: PropTypes.bool
};

export default Modal;