import './App.scss';
import Modal from "./components/Modal";
import Button from "./components/Button";
import React from "react";

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            modal: null
        };

        this.openFirstModal = this.openFirstModal.bind(this);
        this.openSecondModal = this.openSecondModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    closeModal() {
        this.setState({modal: null});
    }

    openFirstModal() {
        this.setState({
            modal:
                <Modal
                    id='1'
                    header='Do you want to delete this file?'
                    text='Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?'
                    actions={
                        [
                            <Button
                                key='Ok'
                                text='Ok'
                                onClick={() => console.log('123')}
                                backgroundColor={'rgba(0, 0, 0, 0.1)'}
                            />,
                            <Button
                                key='Cancel'
                                text='Cancel'
                                backgroundColor={'rgba(0, 0, 0, 0.1)'}
                                onClick={this.closeModal}
                            />
                        ]
                    }
                    closeButton={true}
                    onClose={this.closeModal}
                />
        });
    }

    openSecondModal() {
        this.setState({
            modal:
                <Modal
                    id='1'
                    header='Do you want to continue?'
                    text='Once you continue with React there would be no coming back.'
                    actions={
                        [
                            <Button
                                key='GO'
                                text='GOOO!!!'
                                onClick={this.closeModal}
                                backgroundColor={'green'}
                            />
                        ]
                    }
                    closeButton={false}
                    onClose={this.closeModal}
                />
        })
    }

    render() {
        return (
            <div className="App">
                <Button text='Open first modal' onClick={this.openFirstModal} backgroundColor='blue'/>
                <Button text='Open second modal' onClick={this.openSecondModal} backgroundColor='green'/>
                {this.state.modal}
            </div>
        );
    }
}

export default App;
