import {useEffect, useState} from "react";
import StorageService from "../services/StorageService";
import ProductList from "../components/ProductList";
import ProductService from "../services/ProductService";
import {RenderType} from "../components/Product";

export default function CartPage() {

    const [products, setProducts] = useState([]);

    const fetchProducts = () => {
        ProductService.getProducts().then(res => {
            setProducts(StorageService.getCart().map(id => res.find(product => product.id === id)).filter(Boolean));
        });
    };

    useEffect(() => {
        fetchProducts();
        return () => setProducts([]);
    }, []);

    return <ProductList products={products} renderType={RenderType.cart} updateFunction={fetchProducts} />;
}

CartPage.url = '/cart';