import ProductList from "../components/ProductList";
import {useEffect, useState} from "react";
import ProductService from "../services/ProductService";

export default function ProductsPage() {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        ProductService.getProducts()
            .then(res => setProducts(res));
        return () => setProducts([]);
    }, []);

    return (
        <ProductList products={products}/>
    );
}

ProductsPage.url = '/';