import {useEffect, useState} from "react";
import StorageService from "../services/StorageService";
import ProductList from "../components/ProductList";
import ProductService from "../services/ProductService";

export default function FavoritesPage() {
    const [products, setProducts] = useState([]);

    const fetchProducts = () => {
        ProductService.getProducts().then(res => {
            setProducts(StorageService.getFavorites().map(id => res.find(product => product.id === id)).filter(Boolean));
        });
    };

    useEffect(() => {
        fetchProducts();
        return () => setProducts([]);
    }, []);

    return <ProductList products={products} updateFunction={fetchProducts}/>;
}

FavoritesPage.url = '/favorites';