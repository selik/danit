import './toolbar.scss';
import {NavLink} from "react-router-dom";
import ProductsPage from "../../pages/ProductsPage";
import CartPage from "../../pages/CartPage";
import FavoritesPage from "../../pages/FavoritesPage";

export default function Toolbar() {
    return (
        <div className="toolbar">
            <NavLink exact to={ProductsPage.url}>
                Products
            </NavLink>
            <NavLink exact to={CartPage.url}>
                Cart
            </NavLink>
            <NavLink exact to={FavoritesPage.url}>
                Favorites
            </NavLink>
        </div>
    )
}