import {useEffect, useState} from "react";
import PropTypes from 'prop-types';
import "./product.scss";
import Button from "../Button";
import Modal from "../Modal";
import StorageService from "../../services/StorageService";

export default function Product({product, renderType, index, updateFunction}) {
    const [modalOpened, setModalOpened] = useState(false);
    const [favorite, setFavorite] = useState(false);

    useEffect(() => {
        setFavorite(StorageService.isFavorite(product.id) !== -1);
    }, [product.id]);

    const addToCart = () => {
        StorageService.addToCart(product.id);
        setModalOpened(true);
    };

    const openModal = () => {
        setModalOpened(true);
    };

    const removeFromCart = () => {
        StorageService.removeFromCart(index);
        if (typeof updateFunction === 'function' && renderType === RenderType.cart) {
            updateFunction();
        }
        setModalOpened(false);
    };

    const closeModal = () => {
        setModalOpened(false);
    };

    const onFavoriteClick = () => {
        const newFavState = StorageService.addOrRemoveFavorite(product.id);
        setFavorite(newFavState);
        if (typeof updateFunction === 'function' && renderType === RenderType.list) {
            updateFunction();
        }
    };

    let button;
    switch (renderType) {
        case RenderType.list:
            button = <Button text='Add to Cart' onClick={addToCart} backgroundColor='green'/>;
            break;
        case RenderType.cart:
            button = <Button text='Remove' onClick={openModal} backgroundColor='green'/>;
            break;
        default:
            button = null;
    }

    return (
        <div className='product-tile product-list-tile'>
            <div className='product-tile-img-container'>
                <img className='product-tile-img' src={product.img} alt={product.name}/>
            </div>
            <div className="product-tile-wrapper">
                <div className="product-tile-info">
                    <p className='product-tile-id'><span>ID:</span> {product.id}</p>
                    <p className='product-tile-name'>{product.name}</p>
                    <p className='product-tile-price'>{product.price} UAH</p>
                </div>
                <div className="product-tile-actions">
                    {button}
                    <Button text={favorite ? <i className="fas fa-star"/> : <i className="far fa-star"/>}
                            onClick={onFavoriteClick} backgroundColor='greenyellow'/>
                </div>
            </div>

            {modalOpened ? (
                renderType === RenderType.list ? (
                    <Modal
                        header='The item was added to cart!'
                        text={`Your item ${product.name} was added to cart.`}
                        actions={
                            [
                                <Button
                                    key='Close'
                                    text='Close'
                                    backgroundColor={'rgba(0, 0, 0, 0.1)'}
                                    onClick={closeModal}
                                />
                            ]
                        }
                        closeButton={true}
                        onClose={closeModal}
                    />
                ) : (
                    <Modal
                        header='Confirmation'
                        text={`Are you sure you want to remove product ${product.name} from cart?`}
                        actions={
                            [
                                <Button
                                    key='Confirm'
                                    text='Yes'
                                    backgroundColor={'rgba(0, 0, 0, 0.1)'}
                                    onClick={removeFromCart}
                                />,
                                <Button
                                    key='Close'
                                    text='Close'
                                    backgroundColor={'rgba(0, 0, 0, 0.1)'}
                                    onClick={closeModal}
                                />
                            ]
                        }
                        closeButton={false}
                        onClose={closeModal}
                    />
                )
            ) : null}
        </div>
    )
}

export const RenderType = {
    list: 'List',
    cart: 'Cart'
};

Product.propTypes = {
    product: PropTypes.shape({
        img: PropTypes.string,
        name: PropTypes.string,
        price: PropTypes.number,
        id: PropTypes.number
    }).isRequired,
    renderType: PropTypes.oneOf([RenderType.list, RenderType.cart]),
    updateFunction: PropTypes.func
};