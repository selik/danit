class StorageService {
    constructor() {
        this.addedProducts = 'addedProducts';
        this.favoriteProducts = 'favoriteProducts';
        this.storage = localStorage;
    }

    getFavorites() {
        return JSON.parse(this.storage.getItem(this.favoriteProducts)) || [];
    }

    isFavorite(id) {
        return this.getFavoritesAndIndex(id)[1];
    }

    getFavoritesAndIndex(id) {
        let favoriteProducts = this.getFavorites();
        const index = favoriteProducts.findIndex(v => v === id);
        return [favoriteProducts, index];
    }

    addOrRemoveFavorite(id) {
        const [favoriteProducts, index] = this.getFavoritesAndIndex(id);
        let newFavState = index === -1;

        if (newFavState) {
            favoriteProducts.push(id);
        } else {
            favoriteProducts.splice(index, 1);
        }
        this.storage.setItem(this.favoriteProducts, JSON.stringify(favoriteProducts.sort((a, b) => a - b)));
        return newFavState;
    }

    getCart() {
        return JSON.parse(this.storage.getItem(this.addedProducts)) || [];
    }

    addToCart(id) {
        const addedProducts = this.getCart();
        addedProducts.push(id);
        this.storage.setItem(this.addedProducts, JSON.stringify(addedProducts));
    }

    removeFromCart(index) {
        const addedProducts = this.getCart();
        addedProducts.splice(index, 1);
        this.storage.setItem(this.addedProducts, JSON.stringify(addedProducts));
    }
}

export default new StorageService();