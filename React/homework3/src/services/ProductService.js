class ProductService {
    getProducts() {
        return fetch('products.json')
            .then(r => r.json())
            .then(r => r.products);
    }
}

export default new ProductService();