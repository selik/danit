import './App.scss';
import React from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import ProductsPage from "./pages/ProductsPage";
import Toolbar from "./components/Toolbar";
import CartPage from "./pages/CartPage";
import FavoritesPage from "./pages/FavoritesPage";


export default function App() {
    return (
        <BrowserRouter>
            <div className="App">
                <Toolbar/>
                <Switch>
                    <Route exact path={ProductsPage.url}>
                        <ProductsPage/>
                    </Route>
                    <Route path={CartPage.url}>
                        <CartPage/>
                    </Route>
                    <Route path={FavoritesPage.url}>
                        <FavoritesPage/>
                    </Route>
                </Switch>
            </div>
        </BrowserRouter>
    );
}
