import './App.scss';
import {useEffect} from "react";
import {BrowserRouter} from "react-router-dom";
import Toolbar from "./components/Toolbar";
import RouteConfig from "./config/RouteConfig";
import {useDispatch} from "react-redux";
import {getProducts} from "./store/actions/productsAction";
import ModalConfig from "./config/ModalConfig";

export default function App() {
    let dispatch = useDispatch();

    useEffect(() => {
        dispatch(getProducts());
    }, []);

    return (
        <BrowserRouter>
            <div className="App">
                <Toolbar/>
                <RouteConfig/>
                <ModalConfig/>
            </div>
        </BrowserRouter>
    );
}
