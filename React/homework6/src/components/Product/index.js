import {useEffect, useState} from "react";
import PropTypes from 'prop-types';
import "./product.scss";
import Button from "../Button";
import StorageService from "../../services/StorageService";
import {useDispatch} from "react-redux";
import {closeModal, openModal} from "../../store/actions/modalActions";

export default function Product({product, renderType, index, updateFunction}) {
    const [favorite, setFavorite] = useState(false);
    let dispatch = useDispatch();

    useEffect(() => {
        setFavorite(StorageService.isFavorite(product.id) !== -1);
    }, [product.id]);

    const addToCart = () => {
        StorageService.addToCart(product.id);
        dispatch(openModal({
            type: renderType,
            productName: product.name
        }));
    };

    const removeFromCart = () => {
        dispatch(openModal({
            type: renderType,
            productName: product.name,
            actions: {
                removeFromCart: () => {
                    StorageService.removeFromCart(index);
                    if (typeof updateFunction === 'function' && renderType === RenderType.cart) {
                        updateFunction();
                    }
                    dispatch(closeModal());
                }
            }
        }));
    }

    const onFavoriteClick = () => {
        const newFavState = StorageService.addOrRemoveFavorite(product.id);
        setFavorite(newFavState);
        if (typeof updateFunction === 'function' && renderType === RenderType.list) {
            updateFunction();
        }
    };

    let button;
    switch (renderType) {
        case RenderType.list:
            button = <Button text='Add to Cart' onClick={addToCart} backgroundColor='green'/>;
            break;
        case RenderType.cart:
            button = <Button text='Remove' onClick={removeFromCart} backgroundColor='green'/>;
            break;
        default:
            button = null;
    }

    return (
        <div className='product-tile product-list-tile'>
            <div className='product-tile-img-container'>
                <img className='product-tile-img' src={product.img} alt={product.name}/>
            </div>
            <div className="product-tile-wrapper">
                <div className="product-tile-info">
                    <p className='product-tile-id'><span>ID:</span> {product.id}</p>
                    <p className='product-tile-name'>{product.name}</p>
                    <p className='product-tile-price'>{product.price} UAH</p>
                </div>
                <div className="product-tile-actions">
                    {button}
                    <Button text={favorite ? <i className="fas fa-star"/> : <i className="far fa-star"/>}
                            onClick={onFavoriteClick} backgroundColor='greenyellow'/>
                </div>
            </div>
        </div>
    )
}

export const RenderType = {
    list: 'List',
    cart: 'Cart'
};

Product.propTypes = {
    product: PropTypes.shape({
        img: PropTypes.string,
        name: PropTypes.string,
        price: PropTypes.number,
        id: PropTypes.number
    }).isRequired,
    renderType: PropTypes.oneOf([RenderType.list, RenderType.cart]),
    updateFunction: PropTypes.func
};