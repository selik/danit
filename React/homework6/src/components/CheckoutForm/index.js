import React from 'react';
import {ErrorMessage, Form, Formik} from 'formik';
import * as yup from 'yup';
import {useDispatch, useSelector} from 'react-redux';
import './checkoutForm.scss';
import {checkout} from "../../store/actions/formActions";
import ReduxField from "./ReduxField";

export default function CheckoutForm({products, reload}) {

    let reduxForm = useSelector(state => state.form);
    let dispatch = useDispatch();

    let schema = yup.object().shape({
        firstName: yup.string().required(),
        lastName: yup.string().required(),
        age: yup.number().required().positive().integer(),
        email: yup.string().email().required(),
        address: yup.string().required(),
        phone: yup.string().required()
    });

    return (
        <div className='checkout-form'>
            <Formik
                initialValues={reduxForm}
                onSubmit={data => {
                    dispatch(checkout(data, products, reload));
                }}
                validationSchema={schema}
            >
                {() => {
                    return (
                        <Form>
                            <ReduxField type='text' name='firstName' placeholder='First name'/>
                            <ErrorMessage name='firstName' component='span'/>
                            <ReduxField type='text' name='lastName' placeholder='Last name'/>
                            <ErrorMessage name='lastName' component='span'/>
                            <ReduxField type='text' name='age' placeholder='Age'/>
                            <ErrorMessage name='age' component='span'/>
                            <ReduxField type='email' name='email' placeholder='Email'/>
                            <ErrorMessage name='email' component='span'/>
                            <ReduxField type='text' name='address' placeholder='Address'/>
                            <ErrorMessage name='address' component='span'/>
                            <ReduxField type='text' name='phone' placeholder='Phone'/>
                            <ErrorMessage name='phone' component='span'/>
                            <button type='submit'>Submit</button>
                        </Form>
                    )
                }}
            </Formik>
        </div>
    )
}