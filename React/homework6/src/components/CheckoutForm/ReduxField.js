import {connect, Field} from "formik";
import {onChange} from "../../store/actions/formActions";
import React from "react";
import {useDispatch} from "react-redux";

function ReduxField({type, name, placeholder, formik}) {
    let dispatch = useDispatch();

    return <Field type={type} name={name} placeholder={placeholder} onChange={(e) => {
        formik.handleChange(e);
        dispatch(onChange(e));
    }}/>
}

export default connect(ReduxField);