import React from 'react';
import {shallowToJson} from 'enzyme-to-json';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import AddToCartModal from "./index";
import Button from "../../Button";

configure({adapter: new Adapter()});

test('AddToCartModal is rendered', () => {
    let mock = jest.fn();

    const component = shallow(
        <AddToCartModal data={{productName: 'Test'}} close={mock}/>
    );
    expect(shallowToJson(component)).toMatchSnapshot();

    component.dive().find(Button).simulate('click');
    expect(mock).toBeCalledTimes(1);

    component.dive().find('button.close').simulate('click');
    expect(mock).toBeCalledTimes(2);
});