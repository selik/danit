import React from 'react';
import {shallowToJson} from 'enzyme-to-json';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import AddToCartModal from "./index";
import Button from "../../Button";

configure({adapter: new Adapter()});

test('RemoveFromCartModal is rendered', () => {
    let close = jest.fn();
    let remove = jest.fn();

    const component = shallow(
        <AddToCartModal data={{productName: 'Test', actions: {removeFromCart: remove}}} close={close}/>
    );
    expect(shallowToJson(component)).toMatchSnapshot();

    component.dive().find(Button).first().simulate('click');
    expect(remove.mock.calls.length).toBe(1);

    component.dive().find(Button).last().simulate('click');
    expect(close.mock.calls.length).toBe(1);
});