import Button from "../../Button";
import Modal from "../index";

export default function RemoveFromCartModal({close, data}) {
    return (
        <Modal
            header='Confirmation'
            text={`Are you sure you want to remove product ${data.productName} from cart?`}
            actions={
                [
                    <Button
                        key='Confirm'
                        text='Yes'
                        backgroundColor={'rgba(0, 0, 0, 0.1)'}
                        onClick={data.actions.removeFromCart}
                    />,
                    <Button
                        key='Close'
                        text='Close'
                        backgroundColor={'rgba(0, 0, 0, 0.1)'}
                        onClick={close}
                    />
                ]
            }
            closeButton={false}
            onClose={close}
        />
    )
}