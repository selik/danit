import React from "react";
import PropTypes from 'prop-types';
import './button.scss';

export default function Button({backgroundColor, onClick, text}) {
    return (
        <button className='btn' style={{backgroundColor: backgroundColor}}
                onClick={onClick}>{text}</button>
    )
}

Button.propTypes = {
    text: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired,
    onClick: PropTypes.func.isRequired,
    backgroundColor: PropTypes.string
};

Button.defaultProps = {
    backgroundColor: 'transparent'
};