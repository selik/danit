import React from 'react';
import {shallowToJson} from 'enzyme-to-json';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Button from './index';

configure({adapter: new Adapter()});

test('Button is rendered', () => {
    let mock = jest.fn();

    const component = shallow(
        <Button
            key='Close'
            text='Close'
            backgroundColor='white'
            onClick={mock}
        />
    );
    expect(shallowToJson(component)).toMatchSnapshot();

    component.find('button').simulate('click');
    expect(mock).toBeCalledTimes(1);
});