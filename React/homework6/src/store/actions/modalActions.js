export const OPEN_MODAL = 'modal/addModal';
export const CLOSE_MODAL = 'modal/closeModal';

export function openModal(props) {
    return dispatch => {
        dispatch({
            type: OPEN_MODAL,
            payload: props
        })
    };
}

export function closeModal() {
    return dispatch => {
        dispatch({
            type: CLOSE_MODAL
        })
    };
}