import ProductService from "../../services/ProductService";

export const SAVE_PRODUCTS = "products/SAVE_PRODUCTS";
export const ERR_PRODUCTS = "products/ERR_PRODUCTS";

export function getProducts() {
    return dispatch => {
        ProductService.getProducts()
            .then(products => dispatch(
                {
                    type: SAVE_PRODUCTS,
                    payload: products
                }
            ))
            .catch(err => {
                console.error(err);
                dispatch(
                    {
                        type: ERR_PRODUCTS,
                        payload: err
                    })
            });
    }
}