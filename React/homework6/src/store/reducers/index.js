import {combineReducers} from "redux";
import productsReducer from "./productsReducer";
import modalReducer from "./modalReducer";
import formReducer from "./formReducer";

export default combineReducers({product: productsReducer, modal: modalReducer, form: formReducer});