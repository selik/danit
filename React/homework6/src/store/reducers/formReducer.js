import {CHECKOUT, ON_CHANGE} from "../actions/formActions";

const initialState = {
    firstName: '',
    lastName: '',
    age: '',
    email: '',
    address: '',
    phone: ''
}

export default function formReducer(state = initialState, action) {
    switch (action.type) {
        case ON_CHANGE:
            return {
                ...state,
                ...action.payload
            };
        case CHECKOUT:
            return initialState;
        default:
            return state;
    }
}