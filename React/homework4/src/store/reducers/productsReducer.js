import {ERR_PRODUCTS, SAVE_PRODUCTS} from "../actions/productsAction";

const initialState = {
    products: []
};

export default function productsReducer(state = initialState, action) {
    switch (action.type) {
        case SAVE_PRODUCTS:
            return {...state, products: action.payload};
        case ERR_PRODUCTS:
            return initialState;
        default:
            return state;
    }
}

