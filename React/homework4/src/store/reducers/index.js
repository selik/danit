import {combineReducers} from "redux";
import productsReducer from "./productsReducer";
import modalReducer from "./modalReducer";

export default combineReducers({product: productsReducer, modal: modalReducer});