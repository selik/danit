import {RenderType} from "../../components/Product";
import {useDispatch, useSelector} from "react-redux";
import {closeModal} from "../../store/actions/modalActions";
import AddToCartModal from "../../components/Modal/AddToCartModal";
import RemoveFromCartModal from "../../components/Modal/RemoveFromCartModal";

export default function ModalConfig() {
    const modalOpened = useSelector(state => state.modal.modalOpened);
    const renderType = useSelector(state => state.modal.type);
    const data = useSelector(state => state.modal.data);
    const dispatch = useDispatch();

    const close = () => {
        dispatch(closeModal());
    }

    return modalOpened ? (
        renderType === RenderType.list ? (
            <AddToCartModal close={close} data={data}/>
        ) : (
            <RemoveFromCartModal close={close} data={data}/>
        )
    ) : null;
}