import {Route, Switch} from "react-router-dom";
import ProductsPage from "../../pages/ProductsPage";
import CartPage from "../../pages/CartPage";
import FavoritesPage from "../../pages/FavoritesPage";
import React from "react";

export default function RouteConfig() {
    return (
        <Switch>
            <Route exact path={ProductsPage.url}>
                <ProductsPage/>
            </Route>
            <Route path={CartPage.url}>
                <CartPage/>
            </Route>
            <Route path={FavoritesPage.url}>
                <FavoritesPage/>
            </Route>
        </Switch>
    );
}
