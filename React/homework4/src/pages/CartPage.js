import {useEffect, useState} from "react";
import StorageService from "../services/StorageService";
import ProductList from "../components/ProductList";
import {RenderType} from "../components/Product";
import {useSelector} from "react-redux";

export default function CartPage() {
    const [products, setProducts] = useState([]);
    let allProducts = useSelector(state => state.product.products);

    const fetchProducts = () => {
        setProducts(StorageService.getCart().map(id => allProducts.find(product => product.id === id)).filter(Boolean));
    };

    useEffect(() => {
        fetchProducts();
        return () => setProducts([]);
    }, [allProducts]);

    return <ProductList products={products} renderType={RenderType.cart} updateFunction={fetchProducts}/>;
}

CartPage.url = '/cart';