import ProductList from "../components/ProductList";
import {useSelector} from "react-redux";

export default function ProductsPage() {
    let products = useSelector(state => state.product.products);

    return (
        <ProductList products={products}/>
    );
}

ProductsPage.url = '/';