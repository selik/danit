import {useEffect, useState} from "react";
import StorageService from "../services/StorageService";
import ProductList from "../components/ProductList";
import {useSelector} from "react-redux";

export default function FavoritesPage() {
    const [products, setProducts] = useState([]);
    let allProducts = useSelector(state => state.product.products);

    const fetchProducts = () => {
        setProducts(StorageService.getFavorites().map(id => allProducts.find(product => product.id === id)).filter(Boolean));
    };

    useEffect(() => {
        fetchProducts();
        return () => setProducts([]);
    }, [allProducts]);

    return <ProductList products={products} updateFunction={fetchProducts}/>;
}

FavoritesPage.url = '/favorites';