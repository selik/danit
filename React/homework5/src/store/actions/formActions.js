import StorageService from "../../services/StorageService";

export const ON_CHANGE = "form/ON_CHANGE";
export const CHECKOUT = "form/CHECKOUT";

export function onChange({target: {name, value}}) {
    return dispatch => {
        dispatch({
            type: ON_CHANGE,
            payload: {
                [name]: value
            }
        })
    };
}

export function checkout(data, products, reload) {
    return dispatch => {
        console.log('Submit data', data);
        console.log(StorageService.checkout());
        console.log(products);
        reload();
        dispatch({
            type: CHECKOUT
        })
    };
}