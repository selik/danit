import {OPEN_MODAL, CLOSE_MODAL} from "../actions/modalActions";

const initialState = {
    modalOpened: false,
    data: {}
};

export default function modalReducer(state = initialState, action) {
    switch (action.type) {
        case OPEN_MODAL:
            return {
                modalOpened: true,
                type: action.payload.type,
                data: {
                    productName: action.payload.productName,
                    actions: action.payload.actions
                }
            };
        case CLOSE_MODAL:
            return initialState;
        default:
            return state;
    }
}
