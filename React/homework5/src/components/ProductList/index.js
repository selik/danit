import React from 'react';
import PropTypes from 'prop-types';
import Product, {RenderType} from "../Product";
import './productList.scss';

export default function ProductList({products, renderType, updateFunction}) {
    return (
        <div className='product-list'>
            {products.length > 0 ? products.map((product, index) => <Product product={product}
                                                                             renderType={renderType}
                                                                             index={index}
                                                                             key={"" + index + product.id}
                                                                             updateFunction={updateFunction}/>) : 'No products.'}
        </div>
    );
}

ProductList.propTypes = {
    products: PropTypes.arrayOf(PropTypes.object).isRequired,
    renderType: PropTypes.oneOf([RenderType.list, RenderType.cart]),
    updateFunction: PropTypes.func
};

ProductList.defaultProps = {
    renderType: RenderType.list
};