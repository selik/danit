import Button from "../../Button";
import Modal from "../index";

export default function AddToCartModal({close, data}) {
    return (
        <Modal
            header='The item was added to cart!'
            text={`Your item ${data.productName} was added to cart.`}
            actions={
                [
                    <Button
                        key='Close'
                        text='Close'
                        backgroundColor={'rgba(0, 0, 0, 0.1)'}
                        onClick={close}
                    />
                ]
            }
            closeButton={true}
            onClose={close}
        />
    )
}