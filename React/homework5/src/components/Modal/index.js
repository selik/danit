import React from 'react';
import PropTypes from 'prop-types';
import './modal.scss';

export default function Modal({onClose, header, closeButton, text, actions}) {
    return (
        <div className="modal">
            <div className="modal-overlay" onClick={onClose}/>
            <div className="modal-block">
                <div className="modal-header">
                    <span className='modal-header-text'>{header}</span>
                    {closeButton ?
                        <button className='close' onClick={onClose}>X</button>
                        : null}
                </div>
                <div className="modal-content">
                    {text}
                </div>
                <div className="modal-actions">
                    {actions}
                </div>
            </div>
        </div>
    );
}

Modal.propTypes = {
    header: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    actions: PropTypes.arrayOf(PropTypes.object).isRequired,
    onClose: PropTypes.func.isRequired,
    closeButton: PropTypes.bool
};

Modal.defaultProps = {
    closeButton: true
};