const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

function isValid(book) {
    let missingFields = ['author', 'name', 'price']
        .filter((v => !Object.entries(book).map(([k,]) => k).includes(v)))
        .join(", ");
    if (missingFields.length > 0) {
        throw new Error(missingFields);
    }
    return true;
}

function prepareBooksArray(books) {
    return books.filter((book, i) => {
        try {
            return isValid(book);
        } catch (e) {
            console.error(`Fields "${e.message}" are missing for book ${i}!`);
            return false;
        }
    }).map((book, i) => `<li>Book ${i}: <ul>${prepareBook(book)}</ul></li>`).join("");
}

function prepareBook(book) {
    return Object.entries(book).map(([k, v]) => `<li>${k}: ${v}</li>`).join("");
}

document.getElementById("root").insertAdjacentHTML('afterbegin', `<ul>${prepareBooksArray(books)}</ul>`);