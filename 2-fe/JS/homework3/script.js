class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }
}

let p1 = new Programmer('Sergey', 30, 10000, ['ua']);
let p2 = new Programmer('Christina', 30, 20000, ['ua']);
let p3 = new Programmer('John', 18, 500, ['en']);

console.log(p1, p2, p3);
console.log(p1.salary);