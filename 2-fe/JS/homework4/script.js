function getFilms() {
    fetch('https://swapi.dev/api/films/')
        .then(response => response.json())
        .then(({results: films}) => {
            document.body.innerHTML = `Films: <ul>${films.map(film => `<li>${prepareFilm(film)}</li>`).join('')}</ul>`;
        });
}

function prepareFilm(film) {
    getCharacters(film);
    return `Episode ${film.episode_id} - ${film.title} <ul id="ep${film.episode_id}"><li>${film.opening_crawl}</li><li>Characters:<ul></ul></li></ul>`;
}

function getCharacters(film) {
    Promise.all(film.characters.map(url => {
        return fetch(url)
            .then(response => response.json())
    })).then(list => list
        .forEach(({name}) => document
            .querySelector(`#ep${film.episode_id} li:last-child ul`)
            .insertAdjacentHTML('beforeend', `<li>${name}</li>`)));
}

getFilms();