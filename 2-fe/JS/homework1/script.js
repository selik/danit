function _1() {
    const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
    const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

    let result = [...new Set([...clients1, ...clients2])];
    console.log("1st: ", result);
}

function _2() {
    const characters = [
        {
            name: "Елена",
            lastName: "Гилберт",
            age: 17,
            gender: "woman",
            status: "human"
        },
        {
            name: "Кэролайн",
            lastName: "Форбс",
            age: 17,
            gender: "woman",
            status: "human"
        },
        {
            name: "Аларик",
            lastName: "Зальцман",
            age: 31,
            gender: "man",
            status: "human"
        },
        {
            name: "Дэймон",
            lastName: "Сальваторе",
            age: 156,
            gender: "man",
            status: "vampire"
        },
        {
            name: "Ребекка",
            lastName: "Майклсон",
            age: 1089,
            gender: "woman",
            status: "vempire"
        },
        {
            name: "Клаус",
            lastName: "Майклсон",
            age: 1093,
            gender: "man",
            status: "vampire"
        }
    ];

    let charactersShortInfo = characters.map(({name, lastName, age}) => ({name, lastName, age}));
    // let charactersShortInfo = characters.map(({gender, status, ...rest}) => rest);
    console.log("2nd: ", charactersShortInfo);
}

function _3() {
    const user1 = {
        name: "John",
        years: 30,
    };

    let {name, years, isAdmin = false} = user1;

    console.log("3rd: ", name, years, isAdmin);
}

function _4() {
    const satoshi2020 = {
        name: 'Nick',
        surname: 'Sabo',
        age: 51,
        country: 'Japan',
        birth: '1979-08-21',
        location: {
            lat: 38.869422,
            lng: 139.876632
        }
    };

    const satoshi2019 = {
        name: 'Dorian',
        surname: 'Nakamoto',
        age: 44,
        hidden: true,
        country: 'USA',
        wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
        browser: 'Chrome'
    };

    const satoshi2018 = {
        name: 'Satoshi',
        surname: 'Nakamoto',
        technology: 'Bitcoin',
        country: 'Japan',
        browser: 'Tor',
        birth: '1975-04-05'
    };

    let result = {...satoshi2018, ...satoshi2019, ...satoshi2020};
    console.log("4th: ", result);
}

function _5() {
    const books = [{
        name: 'Harry Potter',
        author: 'J.K. Rowling'
    }, {
        name: 'Lord of the rings',
        author: 'J.R.R. Tolkien'
    }, {
        name: 'The witcher',
        author: 'Andrzej Sapkowski'
    }];

    const bookToAdd = {
        name: 'Game of thrones',
        author: 'George R. R. Martin'
    };

    let result = [...books, bookToAdd];
    console.log("5th: ", result);
}

function _6() {
    const employee = {
        name: 'Vitalii',
        surname: 'Klichko'
    };

    let result = {...employee, age: 30, salary: 5000};
    console.log("6th: ", result);
}

function _7() {
    const array = ['value', () => 'showValue'];

    let [value, showValue] = array;

    alert(value); // должно быть выведено 'value'
    alert(showValue());  // должно быть выведено 'showValue'
}

_1();
_2();
_3();
_4();
_5();
_6();
_7();