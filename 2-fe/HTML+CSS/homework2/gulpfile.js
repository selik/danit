let gulp = require('gulp'),
    sass = require('gulp-sass'),
    del = require('del'),
    htmlmin = require('gulp-htmlmin'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    concat = require('gulp-concat'),
    minify = require('gulp-js-minify'),
    uglify = require('gulp-uglify'),
    purgecss = require('gulp-purgecss'),
    imagemin = require('gulp-imagemin'),
    browserSync = require('browser-sync').create(),
    stream = require('streamqueue');

sass.compiler = require('dart-sass');

let paths = {
    src: {
        html: 'src/*.html',
        scss: 'src/styles/**/*.scss',
        js: 'src/js/*.js',
        img: 'src/img/**/*.png',
        fa: {
            css: 'node_modules/@fortawesome/fontawesome-free/css/all.css',
            js: 'node_modules/@fortawesome/fontawesome-free/js/all.js'
        }
    },
    dist: {
        self: 'dist/*',
        html: 'dist',
        css: 'dist/css',
        js: 'dist/js',
        img: 'dist/img'
    }
};

function clean() {
    return del(paths.dist.self);
}

function buildHtml() {
    return gulp.src(paths.src.html)
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest(paths.dist.html))
        .pipe(browserSync.stream());
}

function buildCss() {
    let sassStream = gulp.src(paths.src.scss)
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        // .pipe(purgecss({
        //     content: ['src/**/*.html']
        // }))
        .pipe(autoprefixer(['> 0.01%', 'last 100 versions']));

    let faStream = gulp.src(paths.src.fa.css)
        .pipe(cleanCSS({compatibility: 'ie8'}));

    return stream({objectMode: true}, sassStream, faStream)
        .pipe(concat('styles.min.css'))
        .pipe(gulp.dest(paths.dist.css))
        .pipe(browserSync.stream());
}

function buildJs() {
    return gulp.src([paths.src.fa.js, paths.src.js])
        .pipe(concat('scripts.min.js'))
        .pipe(minify())
        .pipe(uglify())
        .pipe(gulp.dest(paths.dist.js))
        .pipe(browserSync.stream());
}

function buildImg() {
    return gulp.src(paths.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.dist.img))
        .pipe(browserSync.stream());
}

function watch() {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
    gulp.watch(paths.src.html, buildHtml).on('change', browserSync.reload);
    gulp.watch(paths.src.scss, buildCss).on('change', browserSync.reload);
    gulp.watch(paths.src.js, buildJs).on('change', browserSync.reload);
    gulp.watch(paths.src.img, buildImg).on('change', browserSync.reload);
}

exports.build = gulp.series(clean, buildHtml, buildCss, buildJs, buildImg);
exports.dev = gulp.series(exports.build, watch);