document.addEventListener('DOMContentLoaded', () => {
   document.querySelector('.navbar__action').onclick = function() {
       this.classList.toggle('navbar__action-open');
       document.querySelector('.navbar__list').classList.toggle('navbar__list-open');
   };
});