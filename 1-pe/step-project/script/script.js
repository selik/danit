function shuffle(array) {
    for (let i = array.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}

(function() {
    $('.services-tabs ul').on('click',function (e) {
        $('.services-tabs li.active').removeClass('active');
        $('.services-tabs div.active').removeClass('active');
        $(e.target).addClass('active');
        let currentTab = $(e.target).attr('data-tab');
        $(`.services-tabs div[data-tab=${currentTab}]`).addClass('active');
    });
})();

(function () {
    $('.work-filters').on('click', function (e) {
        let target = e.target;
        if (target.tagName === 'LI') {
            $('.work-filters li.active').removeClass('active');
            $(target).addClass('active');
            applyFilter();
        }
    });

    let loadTimes = 0;

    let types = Array.of(...document.querySelectorAll('.work-tile'))
        .map(e => e.dataset.filter)
        .filter((value, index, self) => self.indexOf(value) === index)
        .filter(v => v);

    let images = Array.of(...document.querySelectorAll('.work-tile img'))
        .map(e => e.getAttribute('src'))
        .filter((value, index, self) => self.indexOf(value) === index)
        .filter(v => v);

    $('.load-work-tiles').on('click', function() {
        $('.our-amazing-work .loader').css('display', '');
        if (++loadTimes === 2) {
            $('.load-work-tiles').remove();
        }

        setTimeout(() => {
            for (let i = 0; i < 12; i++) {
                let clone = $($('.work-tile')[0]).clone();
                clone.removeAttr('id');
                let type = shuffle(types)[0];
                clone.attr('data-filter', type);
                clone.css('display', '');
                clone.find('img').attr('src', shuffle(images)[0]);
                clone.find('p').text('Loaded image ' + ((loadTimes - 1) * 12 + i + 1));
                clone.find('span').text(type.split('-').map(v => v.charAt(0).toUpperCase() + v.slice(1)).join(' '));
                clone.appendTo(".work-tiles");
            }

            applyFilter();
            $('.our-amazing-work .loader').css('display', 'none');
        }, 2000);
    });

    function applyFilter() {
        let targetFilter = $('.work-filters li.active').attr('data-filter');

        if (targetFilter === 'all') {
            $('.work-tiles .work-tile').css('display', 'initial');
        } else {
            $('.work-tiles .work-tile').css('display', 'none');
            $(`.work-tiles .work-tile[data-filter=${targetFilter}]`).css('display', 'initial');
        }
    }
})();

(function () {
    let currentSlideIndex = Array.of(...document.querySelectorAll('.reviews-slider img'))
        .map((v, i) => v.classList.contains('active') ? i : null)
        .filter(e => e)[0] || 0;
    let maxSlideCount = document.querySelectorAll('.reviews-slider img').length - 1;
    let animationProcess = false;

    $('.slider-action').on('click', function (e) {
        let target = e.target;
        if (!animationProcess) {
            animationProcess = true;
            let isLeft = target.classList.contains('slider-left');
            if (isLeft) {
                currentSlideIndex = --currentSlideIndex < 0 ? maxSlideCount : currentSlideIndex;
            } else {
                currentSlideIndex = ++currentSlideIndex > maxSlideCount ? 0 : currentSlideIndex;
            }

            updateReview(currentSlideIndex);
        }
    });

    $('.reviews-slider img').on('click', function (e) {
        let target = e.target;
        if (!animationProcess) {
            animationProcess = true;
            let filterElement = Array.of(...document.querySelectorAll('.reviews-slider img'))
                .map((v, i) => v === target ? i : null)
                .filter(e => e !== null)[0];
            updateReview(filterElement);
        }
    });

    function updateReview(index) {
        $('.reviews-slider img.active').removeClass('active');
        $.when($('.review-block').animate({opacity: 0}, 500))
            .then(function () {
                let slide = document.querySelector(`.reviews-slider img:nth-child(${index + 2})`);
                $('.reviews-review').text(slide.dataset.quote);
                let $reviews = $('.reviews-author');
                $reviews.find('p').text(slide.dataset.author);
                $reviews.find('span').text(slide.dataset.position);
                $reviews.find('img').attr('src', slide.getAttribute('src'));
                $(slide).addClass('active');
                $('.review-block').animate({opacity: 1}, 500);
                animationProcess = false;
            });
    }
})();


window.addEventListener('load', function () {
    let $grid = $('.gallery-grid').masonry({
        itemSelector: '.gallery-grid-item',
        columnWidth: 378,
        gutter: 10
    });

    let images = Array.of(...document.querySelectorAll('.gallery-grid img'))
        .map(e => e.getAttribute('src'))
        .filter((value, index, self) => self.indexOf(value) === index)
        .filter(v => v);

    $('.load-gallery-images').on('click', function () {
        $('.gallery .loader').css('display', '');

        setTimeout(() => {
            let items = [];
            for (let i = 0; i < 6; i++) {
                let clone = $($('.gallery-grid-item')[0]).clone();
                clone.find('img').attr('src', shuffle(images)[0]);
                items.push(...clone.get());
            }

            $grid.append($(items)).masonry('appended', items);

            $('.gallery .loader').css('display', 'none');
        }, 2000);
    });
});


