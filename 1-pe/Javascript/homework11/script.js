document.addEventListener('keypress', (event) => {
    let key = event.code.replace('Key', '');
    document.querySelectorAll('.btn').forEach((value => {
        if (value.innerHTML === key) {
            value.classList.add('btn-active');
        } else {
            value.classList.remove('btn-active');
        }
    }));
});