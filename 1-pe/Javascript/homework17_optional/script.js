let name = promptNonEmptyString("Enter student's name:");
let lastName = promptNonEmptyString("Enter student's last name:");

let student = {
    name,
    lastName,
    report: []
}

let badGradesCount = 0;
let gradesCount = 0;
let gradesSum = 0;

while (true) {
    let courseName = promptNonEmptyString('Enter course name;');
    if (courseName === null) {
        break;
    }
    let courseRating = promptNumber('Enter course rating (0-12):');
    if (courseRating === null) {
        break;
    }

    let course = {
        courseName,
        courseRating
    }

    student.report.push(course);

    gradesSum += courseRating;
    gradesCount++;
    if (courseRating < 4)
        badGradesCount++;
}

/* Task clearly wants me to do this, but there is no need to iterate over report again, we can just count everything at the moment of creation
for (let course of student.report) {
    //...
}*/

if (badGradesCount === 0) {
    alert('Студент переведен на следующий курс');
    let avgGrade = gradesSum / gradesCount;
    if (avgGrade > 7) {
        alert('Студенту назначена стипендия');
    }
} else {
    alert('Course failed');
}

function promptNonEmptyString(s) {
    let n;
    do {
        if (n !== undefined) alert('Your input was invalid');
        n = prompt(s, n);
        if (n === null) {
            return n;
        }
    } while (n.trim() === '')
    return n;
}

function promptNumber(s) {
    let n;
    do {
        if (n !== undefined) alert('Your input was invalid');
        n = prompt(s, n);
        if (n === null) {
            return n;
        }
    } while (n.trim() === '' || Number.isNaN(n) || !Number.isInteger(+n) || n > 12 || n < 1)
    return +n;
}