document.addEventListener('DOMContentLoaded', () => {
    let form = document.getElementById('form');

    form.addEventListener('submit', (e) => {
        e.preventDefault();

        let errorDiv = form.querySelector('.error-msg');

        if (Array.of(...form.elements)
            .filter(i => i.dataset.password !== undefined)
            .filter((value, index, array) => value.value && value.value === array[0].value).length > 1) {
            errorDiv.classList.add('hidden');
            setTimeout(() => alert('You are welcome'), 0);
        } else {
            errorDiv.classList.remove('hidden');
        }
    });

    form.querySelectorAll('.icon-password').forEach(icon => {
        icon.addEventListener('click', () => {
            icon.classList.toggle('fa-eye');
            icon.classList.toggle('fa-eye-slash');

            let input = icon.parentElement.querySelector('input');
            input.type = input.type === 'password' ? 'text' : 'password';
        });
    })
});