let m = promptNumber('1st');
let n = promptNumber('2nd');

let start = Math.min(n, m);
let finish = Math.max(n, m);

let result = [];
outer: for (let i = start; i <= finish; i++) {
    for (let j = 2; j < i; j++) {
        if (i % j === 0) {
            continue outer;
        }
    }
    result.push(i);
}

console.log(result.toString());

function promptNumber(i) {
    let n;
    do {
        if (n !== undefined) alert('Your input was invalid');
        n = prompt(`Enter ${i} number: `);
    } while (n === null || n.trim() === '' || Number.isNaN(n) || !Number.isInteger(+n))
    return +n;
}