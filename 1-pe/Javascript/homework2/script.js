let maxNumber;

do {
    maxNumber = prompt('Enter max number for range: ');
} while (maxNumber === null || maxNumber.trim() === '' || Number.isNaN(maxNumber) || !Number.isInteger(+maxNumber))

let result = [];
for (let i = 1; i <= maxNumber; i++) { // has to start from 1 cause 0 % 5 === 0, thus there is no way 'Sorry, no numbers' would be printed in case if maxNumber > 0.
    if (i % 5 === 0) {
        result.push(i);
    }
}

console.log(result.length !== 0 ? result.toString() : 'Sorry, no numbers');