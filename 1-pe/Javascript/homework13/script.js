(() => {
    let currentTheme = localStorage.getItem('theme');

    if (currentTheme) {
        changeTheme(currentTheme);
    }

    document.addEventListener('DOMContentLoaded', () => {
        let themes = ['css/main.css', 'css/main-alt.css'];
        document.querySelector('.theme-changer').onclick = () => {
            if (currentTheme) {
                let number = themes.indexOf(currentTheme);
                changeTheme(themes[themes.length - number - 1]);
            } else {
                themes.reverse();
                changeTheme(themes[0]);
            }
        };
    });

    function changeTheme(theme) {
        currentTheme = theme;
        let cssLink = document.querySelector('link[data-style]');
        cssLink.href = currentTheme;
        localStorage.setItem('theme', currentTheme);
    }
})();