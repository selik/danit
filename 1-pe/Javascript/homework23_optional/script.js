document.addEventListener('DOMContentLoaded', () => {
    let mines = 10;
    let size = 8;

    let minesArr = [];

    let table = document.createElement('table');
    let counter = 1;
    let total = size ** 2;
    for (let i = size; i > 0; i--) {
        let tr = document.createElement('tr');
        for (let j = 1; j <= size; j++) {
            let td = document.createElement('td');
            td.width = '50px';
            td.height = '50px';

            td.dataset.coordinates = `${j},${i}`;

            if (mines > 0 && Math.random() < (mines / (total - counter))) {
                td.dataset.mine = "";
                minesArr.push(counter);
                mines--;
            }
            td.dataset.id = counter++;

            tr.append(td);
        }
        table.append(tr);
    }
    table.border = '1';
    let body = document.querySelector('body');
    body.append(table);
    body.addEventListener('click', (e) => {
        let target = e.target;
        if (target.tagName === 'TD') {
            checkCell(target);
        }
    });

    function checkCell(cell) {
        let [x, y] = cell.dataset.coordinates.split(',');
        x = +x;
        y = +y;
        let id = +cell.dataset.id;

        console.log(x, y, id);
        if (minesArr.includes(id)) {
            console.log('babax');
        } else {
            let siblilngs = [];
            for (let i = x - 1; i <= x + 1; i++) {
                for (let j = y - 1; j <= y + 1; j++) {
                    if (i !== x || j !== y) {
                        let element = document.querySelector(`td[data-coordinates="${i},${j}"]`);
                        if (element) siblilngs.push({
                            isMine: minesArr.includes(+element.dataset.id),
                            cell: element
                        });
                    }
                }
            }
            console.log(siblilngs);
            cell.innerHTML = siblilngs.filter(v => v.isMine).length;
        }
    }
});