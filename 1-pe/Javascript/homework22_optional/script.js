document.addEventListener('DOMContentLoaded', () => {
    let table = document.createElement('table');
    for (let i = 0; i < 30; i++) {
        let tr = document.createElement('tr');
        for (let j = 0; j < 30; j++) {
            let td = document.createElement('td');
            td.width = '50px';
            td.height = '50px';
            tr.append(td);
        }
        table.append(tr);
    }
    table.border = '1';
    let body = document.querySelector('body');
    body.append(table);
    body.addEventListener('click', (e) => {
        let target = e.target;
        if (target.tagName === 'TD') {
            target.classList.toggle('active');
        } else if (target.tagName === 'BODY') {
            table.classList.toggle('inversed');
        }
    });
});