document.addEventListener('DOMContentLoaded', () => {
    let input = document.getElementById('price');
    let statusDiv = document.querySelector('.status');
    let errorDiv = document.querySelector('.error-msg');

    input.addEventListener('focus', () => {
        clearDiv(errorDiv);
        input.style.color = null;
        input.className = 'active';
    });

    input.addEventListener('blur', () => {
        input.classList.remove('active');

        if (validate(input)) {
            addInfo(input, statusDiv);
            input.style.color = 'green';
        } else {
            addError(input, errorDiv);
            input.className = 'error';
        }
    });

    function validate(input) {
        let value = +input.value;

        return !Number.isNaN(value) && value > 0;
    }

    function addInfo(input, statusDiv) {
        let value = input.value;

        let priceSpan = createElement('span', `Текущая цена: $ ${value}`, 'price-span');

        let xBtn = createElement('button', 'x', 'x-btn');
        xBtn.addEventListener('click', () => {
            priceSpan.remove();
            input.value = null;
            if (statusDiv.children.length === 0) {
                clearDiv(statusDiv);
            }
        });
        priceSpan.append(xBtn);

        statusDiv.classList.remove('hidden');
        statusDiv.append(priceSpan);
    }

    function addError(input, errorDiv) {
        let errorSpan = createElement('span', 'Please enter correct price')

        errorDiv.classList.remove('hidden');
        errorDiv.append(errorSpan);
    }

    function clearDiv(...els) {
        for (let el of els) {
            el.innerHTML = '';
            el.classList.add('hidden');
        }
    }

    function createElement(type, value, cssClass) {
        let element = document.createElement(type);
        element.classList.add(cssClass);
        element.innerHTML = value;
        return element;
    }
});