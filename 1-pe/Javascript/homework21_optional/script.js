document.addEventListener('DOMContentLoaded', () => {
    let btn = document.getElementById('start');
    btn.onclick = () => {
        let input = document.createElement('input');
        document.querySelector('body').prepend(input);
        btn.onclick = () => {
            let value = +document.querySelector('input').value;
            if (!Number.isNaN(value)) {
                draw(value);
            }

            function draw(d) {
                let body = document.querySelector('body');
                body.innerHTML = '';
                let div = createDiv(body, 'display: grid;grid-template-rows: repeat(10, 1fr);grid-template-columns: repeat(10, 1fr);grid-gap: 10px;justify-items: center;', false);
                div.addEventListener('click', (e) => {
                    if (e.target.dataset.type === 'circle') {
                        e.target.remove();
                    }
                });
                for (let i = 0; i < 100; i++) {
                    let color = `rgb(${Math.floor(Math.random() * 255)},${Math.floor(Math.random() * 255)},${Math.floor(Math.random() * 255)})`;
                    createDiv(div, `border:2px solid ${color};background-color:${color};width:${d}px;height:${d}px;border-radius:${d/2}px;`, true);
                }
            }

            function createDiv(body, style, data) {
                let div = document.createElement('div');
                div.style = style;
                if (data) div.dataset.type = 'circle';
                body.append(div);
                return div;
            }
        };
    }
});