###### Опишите своими словами разницу между функциями setTimeout() и setInterval().
setTimeout отвечает за разовое выполнение callback-функции через Х миллисекунд, setInterval - за повтор выполнения callback-функции каждые Х миллисекунд.
###### Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
Callback-функция выполнится после окончания выполнения "текущего" коде, при следующем пуле браузером из message queue, который происходит в среднем каждые 3-5мс. Другими словами, выполнится не моментально, а когда браузер посчитает это возможным. 
###### Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?
Чтобы не занимать ресурсы браузера и не забивать message queue и stack.