document.addEventListener('DOMContentLoaded', () => {
    let i = 1;
    let intervalLength = 10;
    let isTimerActive;
    let timerInterval;
    let interval = createInterval();

    let container = document.querySelector('.images-container');
    let stopBtn = createBtn('Stop', false, container, () => {
        isTimerActive = false;
        stopBtn.setAttribute('disabled', 'true');
        resumeBtn.removeAttribute('disabled');
        clearInterval(interval);
        clearInterval(timerInterval);
        document.querySelector(".timer-container").innerHTML = 'Stopped';
    });
    let resumeBtn = createBtn('Resume', true, container, () => {
        resumeBtn.setAttribute('disabled', 'true');
        stopBtn.removeAttribute('disabled');
        interval = createInterval();
        isTimerActive = true;
    });

    function createInterval() {
        if (!isTimerActive) {
            timerInterval = startTimer(timerInterval);
            return setInterval(() => {
                let images = document.querySelectorAll('.image-to-show');
                images.forEach(el => el.classList.remove('visible'));
                images[i].classList.add('visible');
                i < images.length - 1 ? i++ : i = 0;
                timerInterval = startTimer(timerInterval);
            }, intervalLength * 1000);
        }
    }

    function createBtn(innerHTML, disabled, parent, onclickf) {
        let button = document.createElement('button');
        button.innerHTML = innerHTML;
        if (disabled) {
            button.setAttribute('disabled', 'true');
        }
        parent.append(button);
        button.onclick = onclickf;
        return button;
    }

    function startTimer(oldInterval) {
        if (oldInterval) {
            clearInterval(oldInterval);
        }
        let frame = 100;
        let j = intervalLength * 1000;
        document.querySelector(".timer-container").innerHTML = getTiming(j);
        return setInterval(() => {
            if (j > 1) {
                j -= frame;
                document.querySelector(".timer-container").innerHTML = getTiming(j);
            } else {
                clearInterval(timerInterval);
            }
        }, frame);
    }

    function getTiming(ms) {
        let seconds = Math.floor(ms / 1000);
        let millis = ms - seconds * 1000;
        return `${seconds} seconds ${millis} milliseconds`
    }
});