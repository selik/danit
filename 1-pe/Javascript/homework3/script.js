console.log(calculate(promptNumber('1st'), promptNumber('2nd'), promptOperator()));

function promptNumber(i) {
    let n;
    do {
        if (n !== undefined) alert('Your input was invalid');
        n = prompt(`Enter ${i} operand: `, n);
    } while (n === null || n.trim() === '' || Number.isNaN(n) || !Number.isInteger(+n))
    return +n;
}

function promptOperator() {
    let op;
    let supportedOps = ['+', '-', '*', '/'];
    do {
        if (op !== undefined) alert('Your input was invalid');
        op = prompt('Enter operator: ', op);
    } while (op === null || op.trim() === '' || !supportedOps.includes(op) )
    return op;
}

function calculate(x, y, op) {
    switch (op) {
        case '+': {
            return x + y;
        }
        case '-': {
            return x - y;
        }
        case '*': {
            return x * y;
        }
        case '/': {
            return x / y;
        }
        default: {
            console.log('This should not happen.');
        }
    }
}