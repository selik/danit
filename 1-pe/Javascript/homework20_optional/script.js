function filterCollection(array, terms, isAll, ...fields) {
    if (!array || !Array.isArray(array) || !terms || !fields) {
        throw new Error('missing required parameters!');
    }

    let searchTerms = terms.split(' ').map(el => el.toLowerCase());

    return array.filter(elem => {
        let foundTerms = processObject(elem, "", searchTerms, fields);

        if (isAll) {
            return foundTerms.length === searchTerms.length;
        } else {
            return foundTerms.length > 0;
        }
    });

    function processObject(elem, path, terms, fields) {
        let foundTerms = [];
        for (let key in elem) {
            let currentPath = path + key;
            let value = elem[key];
            if (Array.isArray(value)) {
                for (const el of value) {
                    foundTerms.push(...processObject(el, path + key + ".", terms, fields));
                }
            } else if (typeof value === 'object') {
                foundTerms.push(...processObject(value, path + key + ".", terms, fields));
            } else {
                if (fields.includes(currentPath) && terms.includes(String(value).toLowerCase())) {
                    foundTerms.push(value)
                }
            }
        }
        return foundTerms;
    }


    /*function processElement(elem, searchTerms, field, foundTerms) {
        if (!Array.isArray(elem)) {
            if (typeof field === 'string') {
                let split = field.split('.');
                if (split.length === 1) {
                    let fieldElement = split[0];
                    if (elem.hasOwnProperty(fieldElement)) {
                        if (searchTerms.includes(String(elem[fieldElement]).toLowerCase())) {
                            foundTerms.push(elem[fieldElement]);
                        }
                    }
                } else {
                    let fieldName = split.shift();
                    if (elem.hasOwnProperty(fieldName)) {
                        let rest = split.join('.');
                        processElement(elem[fieldName], searchTerms, rest, foundTerms);
                    }
                }
            } else {
                throw new Error('Wrong field type');
            }
        } else {
            for (let arrEl of elem) {
                processElement(arrEl, searchTerms, field, foundTerms);
            }
        }
    }*/
}

let vehicles = [{
    name: 'Toyota',
    description: 'Corolla',
    contentType: {
        name: 'Car'
    },
    locales: [{
        name: 'en_US',
        description: 'English'
    }, {
        name: 'fr_FR',
        description: 'France'
    }, {
        name: 'es_ES',
        description: 'Spanish'
    }],
    ac: {
        config: {
            zones: 4,
            type: 'auto'
        }
    }
}, {
    name: 'Opel',
    description: 'Corsa',
    contentType: {
        name: 'Car'
    },
    locales: [{
        name: 'fr_FR',
        description: 'France'
    }, {
        name: 'es_ES',
        description: 'Spanish'
    }],
    ac: {
        config: {
            zones: 4,
            type: 'manual'
        }
    }
}, {
    name: 'Mazda',
    description: '3',
    contentType: {
        name: 'Car'
    },
    locales: {
        name: 'en_US',
        description: 'English'
    },
    ac: {
        config: {
            zones: 4,
            type: 'manual'
        }
    }
}];

console.log(1, filterCollection(vehicles, 'en_US Toyota', true, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description'));
console.log(2, filterCollection(vehicles, 'en_US Toyota', false, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description'));
console.log(3, filterCollection(vehicles, 'en_US Corsa', false, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description'));
console.log(4, filterCollection(vehicles, 'en_US Toyota auto', true, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description', 'ac.config.type'));
console.log(5, filterCollection(vehicles, 'en_US Toyota manual', true, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description', 'ac.config.type'));
console.log(6, filterCollection(vehicles, 'en_US', true, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description', 'ac.config.type'));
console.log(7, filterCollection(vehicles, 'bla-bla', false, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description', 'ac.config.type'));
console.log(8, filterCollection(vehicles, 'car', false, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description', 'ac.config.type'));
console.log(9, filterCollection(vehicles, 'car english', true, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description', 'ac.config.type'));
console.log(10, filterCollection(vehicles, 'car english', false, 'ac.config'));
console.log(11, filterCollection(vehicles, '4', false, 'ac.config.zones'));
console.log(12, filterCollection(vehicles, '4', false, null, true));
