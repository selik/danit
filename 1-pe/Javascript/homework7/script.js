function buildArray(arr) {
    build(arr, document.querySelector('.arrays'));
    if (document.querySelector(".timer").innerHTML === '') {
        let i = 10000 / 1000;
        document.querySelector(".timer").innerHTML = i;
        let interval = setInterval(() => {
            if (i > 1) {
                document.querySelector(".timer").innerHTML = --i;
            } else {
                document.querySelector(".arrays").innerHTML = '';
                document.querySelector(".timer").innerHTML = 'Done';
                clearInterval(interval);
            }
        }, 1000);
    }

    function build(arr, parent) {
        let ul = document.createElement('ul');

        for (let el of arr) {
            if (Array.isArray(el)) {
                let li = createLi(ul, 'Inner list: ');
                build(el, li);
            } else {
                createLi(ul, el);
            }
        }

        parent.append(ul);
    }

    function createLi(ul, inner) {
        let li = document.createElement('li');
        li.innerHTML = inner;
        ul.append(li);
        return li;
    }
}
buildArray([['Коля', 'Оля', 'Галя'], ['Степа', ['Вова', 'Серёжа'], 'Макс'], ['Женя', 'Ира', 'Света']]);
buildArray(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);
buildArray(['1', '2', '3', 'sea', 'user', 23]);