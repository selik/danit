function buildArray(arr) {
    document.querySelector('.arrays').insertAdjacentHTML('beforeend', buildMap(arr));

    if (document.querySelector(".timer").innerHTML === '') {
        let i = 10000 / 1000;
        document.querySelector(".timer").innerHTML = i;
        let interval = setInterval(() => {
            if (i > 1) {
                document.querySelector(".timer").innerHTML = --i;
            } else {
                document.querySelector(".arrays").innerHTML = '';
                document.querySelector(".timer").innerHTML = 'Done';
                clearInterval(interval);
            }
        }, 1000);
    }

    function buildMap(arr) {
        return `<ul>${arr.map(el => Array.isArray(el) ? `<li>Inner list: ${buildMap(el)}</li>` : `<li>${el}</li>`).join("")}</ul>`;
    }
}

buildArray([['Коля', 'Оля', 'Галя'], ['Степа', ['Вова', 'Серёжа'], 'Макс'], ['Женя', 'Ира', 'Света']]);
buildArray(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);
buildArray(['1', '2', '3', 'sea', 'user', 23]);

