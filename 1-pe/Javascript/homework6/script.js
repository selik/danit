function filterBy(arr, type) {
    return arr.filter(e => typeof e !== type);
}

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));