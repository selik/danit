function clone(object) {
    if (object) {
        if (Array.isArray(object)) {
            let result = [];
            for (let i of object) {
                result.push(clone(i));
            }
            return result;
        } else if (object instanceof Date) {
            return new Date(object.getTime());
        } else if (typeof object === 'object') {
            let result = {}
            for (let key in object) {
                result[key] = clone(object[key]); // why idea highlights this? Should I check by hasOwnProperty first?
            }
            return result;
        }
    }
    return object;
}

function test(source) {
    let target = clone(source);
    console.log("from: ", source);
    console.log("to: ", target);
    console.log("\r\n");
}

test({name: 'a', last: 'b'});
test(5);
test("test");
test("");
test([]);
test({});
test(null);
test(undefined);
test(true);
test(new Date());
test({a: 'a', b: 2, c: {z: 'x', c: [1, 2, 3], d: new Date()}});