document.addEventListener('DOMContentLoaded', () => {
    let currentTitle, currentContent;

    document.querySelector('.tabs').addEventListener('click', (event) => {
        let _this = event.target;

        let contentName = _this.dataset.content;

        if (contentName) {
            if (currentTitle && currentContent) {
                currentTitle.classList.remove('active');
                currentContent.style.display = 'none';
            }

            currentTitle = _this;
            currentContent = document.querySelector(`.tabs-content li[data-content="${contentName}"]`);

            currentTitle.classList.toggle('active');
            currentContent.style.display = 'list-item';
        }
    });
});