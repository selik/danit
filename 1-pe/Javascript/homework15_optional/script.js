let number = promptNumber();

console.log(factorial(number));

function factorial(n) {
    return n === 1 ? 1 : n * factorial(n - 1);
}

function promptNumber() {
    let n;
    do {
        if (n !== undefined) alert('Your input was invalid');
        n = prompt(`Enter number: `, n);
    } while (n === null || n.trim() === '' || Number.isNaN(n) || !Number.isInteger(+n))
    return +n;
}