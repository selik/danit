let n = promptNumber();

console.log(fibonacci(0, 1, n));

function fibonacci(f0, f1, n) {
    if (n === 0) {
        return f0;
    } else if (n > 0) {
        return fibonacci(f1, f0 + f1, n - 1);
    } else {
        return fibonacci(f1, f0 - f1, n + 1);
    }
}

function promptNumber() {
    let n;
    do {
        if (n !== undefined) alert('Your input was invalid');
        n = prompt(`Enter number: `, n);
    } while (n === null || n.trim() === '' || Number.isNaN(n) || !Number.isInteger(+n))
    return +n;
}