function createNewUser() {
    let firstName = prompt('Enter first name:');
    let lastName = prompt('Enter last name:');

    let user = {
        getLogin: function () {
            return (this.firstName.charAt(0) + this.lastName).toLowerCase();
        },

        setFirstName: function (fn) {
            Object.defineProperty(this, 'firstName', {
                value: fn
            })
        },

        setLastName: function(ln) {
            Object.defineProperty(this, 'lastName', {
                value: ln
            })
        }
    };

    Object.defineProperties(user, {
        firstName: {
            value: firstName,
            writable: false,
            configurable: true
        },
        lastName: {
            value: lastName,
            writable: false,
            configurable: true
        }
    });

    return user;
}

let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
newUser.firstName = '';
newUser.lastName = '';
console.log(newUser);
console.log(newUser.getLogin());
newUser.setFirstName('Sergey');
newUser.setLastName('Tverdokhleb');
console.log(newUser);
console.log(newUser.getLogin());