function createNewUser() {
    // task states that function itself should prompt data, so this is located here rather than outside function
    let firstName = prompt('Enter first name:') || '';
    let lastName = prompt('Enter last name:') || '';

    return {
        firstName,
        lastName,

        getLogin: function () {
            return (this.firstName.charAt(0) + this.lastName).toLowerCase();
        }
    };
}

let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getLogin());