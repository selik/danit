function createNewUser() {
    // task states that function itself should prompt data, so this is located here rather than outside function
    let firstName = promptString('Enter first name:');
    let lastName = promptString('Enter last name:');
    let birthday;
    do {
        let promptBirthday = prompt('Enter birthday (yyyy-mm-dd):');
        if (promptBirthday !== null && promptBirthday.trim() !== '') {
            birthday = new Date(promptBirthday); // doesn't check if date is indeed in 'yyyy-mm-dd' format, just accepts 'yyyy' and 'yyyy-mm'. moment.js could be used for validation :D
        }
    } while (!birthday || Number.isNaN(birthday.getTime()))

    return {
        firstName,
        lastName,
        birthday,

        getLogin: function () {
            return (this.firstName.charAt(0) + this.lastName).toLowerCase();
        },

        getAge: function () {
            return Math.floor((Date.now() - birthday.getTime()) / 1000 / 60 / 60 / 24 / 365);
        },

        getPassword: function () {
            return firstName.charAt(0).toUpperCase() + lastName.toLowerCase() + birthday.getFullYear();
        }
    };
}

let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());

function promptString(s) {
    let res;
    do {
        if (res !== undefined) alert('Your input was invalid');
        res = prompt(s, res);
    } while (res === null || res.trim() === '')
    return res;
}