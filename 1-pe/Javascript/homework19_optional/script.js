function checkDeadline(velocity, backlog, deadline) {
    velocity = velocity || [];
    backlog = backlog || [];
    deadline = deadline || new Date();

    let springLength = workingDaysBetween(new Date(), deadline);

    let velocitySum = 0;
    let backlogSum = 0;

    for (let em of velocity) {
        if (typeof em === 'number') {
            velocitySum += em;
        }
    }

    for (let em of backlog) {
        if (typeof em === 'number') {
            backlogSum += em;
        }
    }

    let neededDays = backlogSum / velocitySum;
    let result = springLength - neededDays;

    return result >= 0 ?
        `Все задачи будут успешно выполнены за ${Math.floor(result)} дней до наступления дедлайна!` :
        `Команде разработчиков придется потратить дополнительно ${Math.abs(result) * 8} часов после дедлайна, чтобы выполнить все задачи в беклоге`;
}

function workingDaysBetween(start, end) {
    let result = 0;
    while (start < end) {
        let day = start.getDay();
        if (day !== 0 && day !== 6) {
            result++;
        }
        start.setDate(start.getDate() + 1);
    }
    return result;
}


let today = new Date();
console.log(today);
end = new Date(today);
end.setDate(end.getDate() + 1);
console.log(end);

console.log(checkDeadline([8, 2], [2, 3, 5], end));