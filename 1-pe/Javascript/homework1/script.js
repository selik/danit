let name, age;

do {
    name = prompt('Enter your name please: ', name);
    age = prompt('Enter your age please: ', age);
} while (name === null || age === null || name.trim() === '' || age.trim() === '' || Number.isNaN(+age))

if (age < 18) {
    alert('You are not allowed to visit this website.');
} else if (age >= 18 || age <= 22) {
    if (confirm('Are you sure you want to continue?')) {
        alert(`Welcome, ${name}`);
    } else {
        alert('You are not allowed to visit this website');
    }
} else {
    alert(`Welcome, ${name}`);
}